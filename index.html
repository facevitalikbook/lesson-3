<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Title</title>
</head>
<body>
<div>
    <h1>Using constraints & “magic numbers” to build responsive layouts in Figma</h1>
    <img src="https://miro.medium.com/max/1100/1*Y2dJzfk5qWf-vI1MYiiTVw.png">
    <p style="text-align: center">Image created for Figma by Peter Barnaba</p>
    <p>
        <i>This post is part of a series: <a
                href="https://medium.com/@littlebits.product/building-a-design-system-with-figma-at-littlebits-17edff8f4236">Building
            a design system with Figma at littleBits.</a> Read the previous post — how we created atomic components with
            Figma — <a href="https://blog.figma.com/creating-responsive-layout-templates-in-figma-e310f02a06cc">here</a>.</i>
        <br>
        <br>
        This year we faced a big challenge at Little Bits — launching 4 new apps on multiple platforms with both mobile
        and tablet layouts. We knew we needed a strong design system to do it right, particularly given our small team
        and short time span (6 months).
        <br>
        <br>

        Templates with responsive layouts played a key role in scaling our work. Here’s how we figured out the approach
        that worked for us in both Figma and React.
        <br>
        <br>

    <h2>Magic Number</h2>
    <mark>One thing that’s great to have at the heart of a design system is a ‘magic number’ — it helps harmonize layout
        if sizes of text, graphic elements, margins, and padding, share a common numeric factor.
    </mark>
    <br>
    <br>

    Before we had any kind of system, we had mocks of several key screens that had been created to establish the visual
    direction of the app.
    <br>
    <img src="https://miro.medium.com/max/1100/1*TqFz6lS9kuTmok-clI-X1Q.png">
    <p style="text-align: center">Initial ‘eyeballed’ design mock compared with eventual design using layout based off 8's</p>
    We looked at the sizes and padding that had been ‘eyeballed’ in these designs, and found that things were mostly
    working off 8’s — buttons were 48px high, margins were mostly 24px, some headings were 32px, etc.
    <br>
    <br>
    Everything was close enough to nudge towards numbers that multiplied from 8 (and 8 in itself is a number that
    multiplies and divides well!). So 8 became our magic number for all things moving forward.
    <br>
    <img src="https://miro.medium.com/max/1100/1*kmKS2fqTI7jtkDDdwm_VjQ.png">
    <p style="text-align: center">Buttons built on a scale of 8</p>
    From smallest to largest our buttons and form elements were made 32, 48, 56, 64, & 96px high; key headings & copy
    48, 32, 24, & 16px; padding & gutters 16, 24, & 32px; larger layout cards would be 240px high.
    <br>
    <br>
    You get the picture…all the key sizes are multiples of 8!
    <br>
    <br>
    <h2>Different Screen Sizes</h2>
    Figma is great for building with responsive constraints — you can stretch your layouts and see how they will respond
    to changes in screen size. (If you’re new to constraints in Figma, check out <a
        href="http://8%20became%20our%20magic%20number%20for%20all%20things%20moving%20forward./"> this beginner
    primer</a> here.)
    <br>
    <br>
    You can even have elements pin to edges of a column:
    <br>
    <br>
    <img src="https://miro.medium.com/max/1100/1*dFGDlSVyHIKEtHyrcMGxHA.gif">
    <p style="text-align: center">Pin elements to a column in Figma using constraints</p>
    But it’s hard to create a single layout that will look good on most phones, still fit on a smaller phone like an
    iPhone SE, and feel full on a tablet.
    <br>
    <br>
    <img src="https://miro.medium.com/max/1100/1*-tgO36tfC_joP2YCHuKZhA.png">
    <p style="text-align: center">Controls layouts on different screens with nothing but layout constraints.</p>
    <br>
    Around half our users are loading the app on a tablet, so we didn’t want the experience there to feel like an
    afterthought. We didn’t want our designs to be compromised to fit with smaller screens either.
    <br>
    <br>
    In this case our designs were sympathetic to being proportionally scaled up and down, so we looked at the
    relationship between different screen sizes to get scaling ratios:
    <br>
    <br>
    <img src="https://miro.medium.com/max/1100/1*GzOYNQg-YmBExV_6o_kihg.png">
    <br>
    Then we rounded out the ratio values so we could keep whole numbers as we scaled:
    <br>
    <br>
    <img src="https://miro.medium.com/max/1100/1*UnBgpVjRk01Gbapzh9Vxng.png">
    <br>
    <br>
    We baked these rules into how the React codebase calculated sizes (along with typical layout constraints).
    <br>
    <br>
    Because our templates were designed with this system in mind, when we tested the final build on tablets and smaller
    phones, things looked great right out of the box without needing extra adjustments.
    <br>
    <br>
    In our Figma designs we were able to have just one template design for each screen, and use a mixture of the scale
    tool and frame resizing to get a preview of how things would look on different devices.
    <br>
    <br>
    <img src="https://miro.medium.com/max/1100/1*ExxKlGR1Pl3dEzd9xocIJA.png">
    <p style="text-align: center">Controls layouts with constraints and scaling rules applied.</p>
    <br>
    There were other responsive design challenges to solve. The app had hundreds of videos and animations, we didn’t
    want to have to double handle multiple versions of the content files for different screen dimensions.
    <br>
    <br>
    We created all our video / animation content to work in 4:3 but with safe areas that could be cropped for 16:9
    screens.
    <br>
    <br>
    <img src="https://miro.medium.com/max/1100/1*VsTC2wy4CJEPIIQUCLEsNg.png">
    <p style="text-align: center">4:3 Content Cropped on 16:9</p>
    <br>
    <br>
    <h2>Text Sizes & Internationalization</h2>
    With a ‘magic number’, sometimes there will need to be exceptions — with text in particular at smaller sizes a few
    pixels makes a lot of difference. But where 8’s didn’t fit, we tried to fall back to 4’s — so some text sizes were
    12 & 20px.
    <br>
    <br>
    As well as needing variations for smaller copy, we were translating the app for 6 languages. Ideal text sizes were
    set in the templates for headings etc, but if a (German!) word didn’t fit within the available space, we created
    code to scale it down to the next heading size that would fit.
    <br>
    <br>
    <img src="https://miro.medium.com/max/1100/1*-8FrkflE7egP4wnkCtIilg.png">
    <br>
    <br>
    So we ended up with a larger number of heading sizes than we used in the primary designs, but we kept a smooth
    transition of sizes when dropping from H1 to H6.
    <br>
    <br>
    <br>
    <h2>Summary</h2>
    Most of our layouts are reasonably simple — centered elements, or 2–3 columns of content. We were lucky not to need
    responsive patterns besides the scaling system and basic layout constraints.
    <br>
    <br>
    The ‘magic number’ helps the design feel tight, it saves time when deciding how to position, size and space things,
    and of course it is much easier to code up a design that follows well defined consistent rules.
    <br>
    <br>
    Next step was to figure out the <a
        href="https://medium.com/@littlebits.product/creating-themed-versions-of-a-master-design-in-figma-3d6679ffdeb7">
    correct workflow for our themes.</a>
    <br>
    <br>
    This post is part of a series <a
        href="https://medium.com/@littlebits.product/building-a-design-system-with-figma-at-littlebits-17edff8f4236">
    Building a Design System with Figma at littleBits.</a> Read the next post <a
        href="https://medium.com/@littlebits.product/creating-themed-versions-of-a-master-design-in-figma-3d6679ffdeb7">
    Creating Themed Versions of a Master Design in Figma.</a>


</div>
</body>
</html>